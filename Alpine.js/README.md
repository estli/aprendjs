# Alpine.js - Un mini Framework de JavaScript

## Este framework no necesita de Node.js para ser utilizado

Esta inspirado en este Framework CSS [Tailwind](https://tailwindcss.com/) para JavaScript.

#### Funcionamiento

> Este repo contiene Express.js porque es necesario un servidor para que Alpine pueda funcionar

**Con npm:** Instalar paquetes desde npm
```js
npm i //De lo contrario no funciona
node app.js //Para activar el secvidor Express.js
```
**Ahora solo es ingresar a http://localhost:3000/index.html**

#### Instalacion

> Se puede instalar mediante npm, pero es opcional

**Con npm:** Instalar el paquete con npm
```js
npm i alpinejs
```
**Con CDN:** Agregar la siguiente linea de codigo en la seccion `<head>` .
```html
<script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>
```

**Con Archivos:** Usando la carpeta 'Public' y colocarlos en tu servidor favorito.
```html
<script src="/dist/alpine.js"></script>
```

#### Documentacion Oficial

[Documentacion Alpine.js](https://github.com/alpinejs/alpine/)

**Este repo fue creado por Marco Antonio Castillo Reyes**