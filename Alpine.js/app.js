
var express = require('express');
var app = express();

app.get('/', function (req, res) {
  res.send('Visita http://localhost:3000!');
});

app.use( express.static('public'));

app.listen(3000, function () {
  console.log('App escuchando desde el puerto 3000!');
});

